// configure accounts
Accounts.ui.config({
    passwordSignupFields: 'USERNAME_AND_EMAIL'
});

// get all users
Template.allUsers.users = function() {
    return Meteor.users.find({});
};

// get all messages
Template.chatBox.messages = function() {
    return Messages.find( {}, { sort: { time: -1 }} );
}

// add new message
var newMessage = function() {
    var input = document.getElementById('message-input');

    if(input.value !== '') {
        Messages.insert({
            author: Meteor.user(),
            body: input.value,
            time: Date.now()
        });

        input.value = '';
    }
}

// add message events
Template.chatBox.events = {
    'keydown #add-message-form input': function(e) {
        if(e.which === 13) {
            newMessage();
        }
    },

    'click #add-message-form button': function(e) {
        e.preventDefault();
        newMessage();
    }
}

// helper to display date formated
UI.registerHelper("formatTime", function(context) {
    return moment(new Date(context)).fromNow();
});


